package fakultaet;

import static org.junit.Assert.*;

import org.junit.Test;

public class FakultaetTest {
	
	

	@Test
	public void testBerechne() {
	    assertEquals(1, Fakultaet.berechne(0));
	    assertEquals(1, Fakultaet.berechne(1));
	    assertEquals(120, Fakultaet.berechne(5));
	}

}
